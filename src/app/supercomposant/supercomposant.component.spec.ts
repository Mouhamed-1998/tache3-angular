import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupercomposantComponent } from './supercomposant.component';

describe('SupercomposantComponent', () => {
  let component: SupercomposantComponent;
  let fixture: ComponentFixture<SupercomposantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupercomposantComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SupercomposantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
