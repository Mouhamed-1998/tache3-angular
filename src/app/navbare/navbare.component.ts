import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthserviceComponent } from '../authentification/shared/authservice/authservice.component';

@Component({
  selector: 'app-navbare',
  templateUrl: './navbare.component.html',
  styleUrls: ['./navbare.component.css']
})
export class NavbareComponent implements OnInit {

@Output() sideNavToggled = new  EventEmitter<boolean>();
menuStatus : boolean = false;

  constructor(private auth : AuthserviceComponent){}

  ngOnInit(): void {
    
  }
register(){
  this.auth.logout();
}

SideNavToggle(){
  this.menuStatus = !this.menuStatus;
  this.sideNavToggled.emit(this.menuStatus)
}
}
