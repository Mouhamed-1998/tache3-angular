import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore"
import { Categorie } from '../model/categorie';
import { Produit } from '../model/produit';
import { Users} from "../model/users"


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private afs : AngularFirestore ) { }

  //add users
  addUser(user : Users){
user.id = this.afs.createId();
return this.afs.collection('/Utilisateur').add(user)
  }

  //get all student
  getAllUser(){
    return this.afs.collection('/Utilisateur').snapshotChanges(); 
  }
  getAllUsere(users : Users){
    return this.afs.collection('/Utilisateur/'+users.id).snapshotChanges(); 
  }

  //delete
  deleteUser(users : Users){

    return this.afs.doc('/Utilisateur/'+users.id).delete();
  }

  //Update users/
  Updateuser(users : Users){
    return this.afs.doc('/Utilisateur/'+users.id).update({
    nom : users.nom,
    prenom : users.prenom,
    email : users.email,
    password : users.password

    });
  }

updateUser(users : Users){

this.deleteUser(users)
this.addUser(users)
  }


  // Categorie

  //add users
  addCategorie( categorie : Categorie){
    categorie.id = this.afs.createId();
    return this.afs.collection('/Categories').add(categorie)
      }
    
      //get all student
      getAllCategorie(){
        return this.afs.collection('/Categories').snapshotChanges(); 
      }
    
      //delete
      deleteCategorie( categorie : Categorie){  
       return this.afs.doc('/Categories/'+categorie.id).delete();
      }
    
      //Update users/
    
    updateCategorie(categorie : Categorie){
    
    this.deleteCategorie(categorie)
    this.addCategorie(categorie)
      }
    

      //Produit

      //add users
  addProduit( produit : Produit){
    produit.id = this.afs.createId();
    return this.afs.collection('/Produits').add(produit)
      }
    
      //get all student
      getAllProduit(){
        return this.afs.collection('/Produits').snapshotChanges(); 
      }
    
      //delete
      deleteProduit( produit : Produit){
    
        return this.afs.doc('/Produits/'+produit.id).delete();
      }
    
      //Update users/
    
    updateProduit(produit : Produit){
    
    this.deleteProduit(produit)
    this.addProduit(produit)
      }

      

}
