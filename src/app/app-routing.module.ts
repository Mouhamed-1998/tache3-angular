import { NgModule } from "@angular/core";
import { RouterModule,Routes } from "@angular/router";
import {CategorieComponent  } from "./tache/categorie/categorie.component";
import {DashbordComponent  } from "./tache/dashbord/dashbord.component";
import { ProductComponent } from "./tache/product/product.component";
import { UsersComponent } from "./tache/users/users.component";
import { ConnectionComponent } from "./authentification/connection/connection.component";
import { InscriptionComponent } from "./authentification/inscription/inscription.component";
import {SupercomposantComponent} from "./supercomposant/supercomposant.component"

const routes: Routes = [
    {path:'', redirectTo:'connecte', pathMatch:'full' },
    {path:'connecte', component :ConnectionComponent },
    {path:'superdashboard', component :SupercomposantComponent },
    {path:'dashboard', component :DashbordComponent },
    {path:'inscription', component :InscriptionComponent },
    {path:'user', component : UsersComponent },
    {path:'produit', component :ProductComponent },
    {path:'categorie', component :CategorieComponent }
   
];
@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports:[RouterModule]
})
export class AppRoutingModule {}