import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebare',
  templateUrl: './sidebare.component.html',
  styleUrls: ['./sidebare.component.css']
})
export class SidebareComponent implements OnInit{
@Input()  sideNavStatus: boolean = false;
  
list = [{
  number : '1',
  name : 'Dashbord',
  path :'/dashboard',
  icon : 'fas fa-warehouse',
},
{
  number : '2',
  name : 'Users',
  path :'/user',
  icon : 'fas fa-user-plus',
},
{
  number : '3',
  name : 'Produits',
  path :'/produit',
  icon : 'fas fa-shopping-cart',
},
{
  number : '4',
  name : 'Catégories',
  path :'/categorie',
  icon : 'fas fa-chart-bar'
}
]; 

  constructor(){}
  ngOnInit(): void {
    
  }

}
