import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ToastrService } from 'ngx-toastr';
import { Users } from 'src/app/model/users';
import { DataService } from "../../shared/data.service"
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  sideNavStatus: boolean = true;

  userList : Users[] = [];
userObj : Users = {
  id: '',
  nom: '',
  prenom: '',
  email: '',
  password: ''
};

  id : string ='';
  nom : string ='';
  prenom : string ='';
  email : string ='';
  password : string ='';



  constructor( private data : DataService ,private afs : AngularFirestore ,private toastr: ToastrService ){ }
  ngOnInit(): void {
    this.getAllUsers();
    
  }

getAllUsers(){
  
  this.data.getAllUser().subscribe(res=>{
    this.userList = res.map((e: any)=>{
      const data = e.payload.doc.data();
      data.id = e.payload.doc.id;
      return data;
    })

  },err=>{
    alert("Error")
  }
  )
}

getAllModaluser(users : Users){
  
  this.id=users.id,
  this.nom= users.nom,
  this.prenom= users.prenom,
   this.email= users.email,
   this.password= users.password
}

resetForm(){
 this.id='',
 this.nom= '',
 this.prenom= '',
  this.email= '',
  this.password= ''
}

addUsers(){
  if (this.nom ==''|| this.prenom ==''|| this.email ==''|| this.password =='') {
 
   this.toastr.warning('Veillez saisir tout les champs !', 'Avertissement', {
    timeOut: 3000,
  });
   return
  }
  this.userObj.id = ''
  this.userObj.nom = this.nom
  this.userObj.prenom = this.prenom
  this.userObj.email = this.email
  this.userObj.password = this.password

  this.data.addUser(this.userObj)

this.resetForm()
this.toastr.success(' Add successful!', 'ok', {
  timeOut: 3000,
});

}

updateUser(){
 try {
 
  this.afs.collection('Utilisateur').doc(this.id).update({
    nom : this.nom,
    prenom : this.prenom,
    email : this.email,
    password : this.password
    })
    this.toastr.success('Update successful', 'ok', {
      timeOut: 3000,
    });
    
     this.resetForm()
    } catch (error) {
      alert(error)
    }
}

deleteUsers(users : Users){
  if(window.confirm('etes vous sur de vouloir supprimer '+users.prenom+ " "+users.nom+"?"))
  this.data.deleteUser(users)
}
}
