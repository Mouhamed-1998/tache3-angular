import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ToastrService } from 'ngx-toastr';
import { Categorie } from 'src/app/model/categorie';
import { Produit } from 'src/app/model/produit';
import { DataService } from 'src/app/shared/data.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  sideNavStatus: boolean = true;

  categorieList : Categorie[] = [];
  produitList : Produit[] = [];
  produitObj : Produit = {
    id : '',
    nomProduit :'',
    Categorie :' ',
    prixProduit :''
  };
  
    id : string ='';
    nomProduit : string ='';
    Categorie : string =' ';
    prixProduit : string=''
  
    selected :string= ''

    constructor(private  data : DataService,private afs : AngularFirestore , private toastr: ToastrService  ){}
   
    ngOnInit(): void {
      this.getAllProduits();
      this.getAllCategories();
    }
  
    getAllProduits(){
      this.data.getAllProduit().subscribe(res=>{

        this.produitList = res.map((e: any)=>{
          const data = e.payload.doc.data();
          data.id = e.payload.doc.id;
          return data;
        })
    
      },err=>{
        alert("Error")
      }
      )
    }

    getAllModalProduits(produit : Produit){
      
      this.id  =produit.id,
      this.nomProduit  =produit.nomProduit,
      this.Categorie =produit.Categorie,
      this.prixProduit =produit.prixProduit
      }


    getAllCategories(){
      this.data. getAllCategorie().subscribe(res=>{

        this.categorieList = res.map((e: any)=>{
          const data = e.payload.doc.data();
          data.id = e.payload.doc.id;
          return data;
        })
    
      },err=>{
        alert("Error")
      }
      )
    }

   
    resetForm(){
      this.id  ='',
      this.nomProduit  ='',
      this.Categorie ='',
      this.prixProduit =''
     }

     addProduits(){
      if (this.nomProduit ==''|| this.Categorie ==''|| this.prixProduit =='') {
        this.toastr.warning('Veillez saisir tout les champs !', 'Avertissement', {
          timeOut: 3000,
        });
       return
      }
      this.produitObj.id = this.id
      this. produitObj.nomProduit = this.nomProduit
      this. produitObj.Categorie = this.Categorie
      this. produitObj.prixProduit = this.prixProduit
      
      this.data.addProduit(this.produitObj)
    
      this.resetForm()
      this.toastr.success(' Add successful!', 'ok', {
        timeOut: 3000,
      });
    
    }
    
    updateProduit(){
      try {
 
        this.afs.collection('Produits').doc(this.id).update({
          nomProduit : this.nomProduit,
          Categorie : this.Categorie, 
          prixProduit : this.prixProduit
          })
          this.toastr.success('Update successful', 'ok', {
            timeOut: 3000,
          });
           this.resetForm()
          } catch (error) {
            alert(error)
          }

    }


    deleteProduits(produit : Produit){
      if(window.confirm('etes vous sur de vouloir supprimer le produit '+produit.nomProduit+ "?"))
      this.data.deleteProduit(produit)
    }
    // alert( " "+produitcategorieList+" ")
    
}
