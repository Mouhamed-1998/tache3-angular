import { Component, OnInit } from '@angular/core';
import { Categorie } from 'src/app/model/categorie';
import { Produit } from 'src/app/model/produit';
import { Users } from 'src/app/model/users';
import { DataService } from 'src/app/shared/data.service';

@Component({
  selector: 'app-dashbord',
  templateUrl: './dashbord.component.html',
  styleUrls: ['./dashbord.component.css']
})
export class DashbordComponent implements OnInit {
  sideNavStatus: boolean = true;
  categorieList : Categorie[] = [];
  userList : Users[] = [];
  ProduitList : Produit[] = [];

  constructor(private  data : DataService){}

  ngOnInit(): void {
    this. getAllUsers()
    this.getAllProduits()
   this.getAllCategories()
  }
  getAllUsers(){

    this.data.getAllUser().subscribe(res=>{
  
      this.userList = res.map((e: any)=>{
        const data = e.payload.doc.data();
        data.id = e.payload.doc.id;
        return data;
      })
  
    },err=>{
      alert("Error")
    }
    )
  }

  getAllProduits(){

    this.data.getAllProduit().subscribe(res=>{
  
      this.ProduitList = res.map((e: any)=>{
        const data = e.payload.doc.data();
        data.id = e.payload.doc.id;
        return data;
      })
  
    },err=>{
      alert("Error")
    }
    )
  }

  getAllCategories(){

    this.data.getAllCategorie().subscribe(res=>{
  
      this.categorieList = res.map((e: any)=>{
        const data = e.payload.doc.data();
        data.id = e.payload.doc.id;
        return data;
      })
  
    },err=>{
      alert("Error")
    }
    )
  }
  

}
