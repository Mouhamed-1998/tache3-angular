import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ToastrService } from 'ngx-toastr';
import { Categorie } from 'src/app/model/categorie';
import { DataService } from 'src/app/shared/data.service';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.css']
})
export class CategorieComponent implements OnInit {
  sideNavStatus: boolean = true;

  categorieList : Categorie[] = [];
  categorieObj : Categorie = {
    id: '',
    nomCategorie: '',
    description: ''
  };
  
    id : string ='';
    nomCategorie : string ='';
    description : string ='';
 
  constructor(private  data : DataService ,private afs : AngularFirestore ,private toastr: ToastrService ){
   
  }
  
  ngOnInit(): void {
    this.getAllCategories();
  }
  
  getAllCategories(){
    this.data.getAllCategorie().subscribe(res=>{

      this.categorieList = res.map((e: any)=>{
        const data = e.payload.doc.data();
        data.id = e.payload.doc.id;
        return data;
      })
  
    },err=>{
      alert("Error")
    }
    )
  }

  getAllModalCategories(categorie : Categorie){
    this.id  =categorie.id,
    this.nomCategorie  =categorie.nomCategorie,
    this.description  =categorie.description
  }

  resetForm(){
   this.id  ='';
    this.nomCategorie  ='';
    this.description  ='';
  }

  addCategories(){
    if (this.nomCategorie ==''|| this.description =='') {
     alert("Viellez saisir tout les champs !") 
     return
    }
    this. categorieObj.id = this.id,
    this. categorieObj.nomCategorie = this.nomCategorie
    this. categorieObj.description = this.description
    
    this.data.addCategorie(this.categorieObj)
  
  this.resetForm()
  this.toastr.success(' Add successful!', 'ok', {
    timeOut: 3000,
  });
  
  }
  updateCategorie(){
    try {
 
      this.afs.collection('Categories').doc(this.id).update({
        nomCategorie : this.nomCategorie,
        description : this.description
       
        })
        this.toastr.success('Update successful', 'ok', {
          timeOut: 3000,
        });
         this.resetForm()
        } catch (error) {
          alert(error)
        }
  }
  
 
  
  deleteCategories(categorie : Categorie){
    if(window.confirm('etes vous sur de vouloir supprimer une categorie '+categorie.nomCategorie+ "?"))
    this.data.deleteCategorie(categorie)
  }
}
