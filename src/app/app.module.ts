import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InscriptionComponent } from './authentification/inscription/inscription.component';
import { ConnectionComponent } from './authentification/connection/connection.component';
import { CategorieComponent } from './tache/categorie/categorie.component';
import { ProductComponent } from './tache/product/product.component';
import { UsersComponent } from './tache/users/users.component';
import { DashbordComponent } from './tache/dashbord/dashbord.component';
import{ AngularFireModule } from "@angular/fire/compat"
import { NavbareComponent } from './navbare/navbare.component';
import { SidebareComponent } from './sidebare/sidebare.component';
import { environment } from 'src/environment/environment';
import { AuthserviceComponent } from './authentification/shared/authservice/authservice.component';
import { FormsModule } from '@angular/forms';
import { SupercomposantComponent } from './supercomposant/supercomposant.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { AngularFirestore } from "@angular/fire/compat/firestore";



@NgModule({
  declarations: [
    AppComponent,
    InscriptionComponent,
    ConnectionComponent,
    CategorieComponent,
    ProductComponent,
    UsersComponent,
    DashbordComponent,
    NavbareComponent,
    SidebareComponent,
    AuthserviceComponent,
    SupercomposantComponent,
  
   

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
