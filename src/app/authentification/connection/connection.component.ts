import { Component, OnInit } from '@angular/core';
import { AuthserviceComponent } from '../shared/authservice/authservice.component';


@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.css']
})
export class ConnectionComponent implements OnInit {

 email : string='';
 password : string ='' 
  constructor(private auth :AuthserviceComponent){
    
  }
  ngOnInit(): void {
    
  }

  login(){
    if(this.email == ''){
      alert('entrer votre email svp !')
      return;
    }
    if(this.password == ''){
      alert('entrer votre password svp !')
      return;
    }
    this.auth.login(this.email,this.password);
    this.email='';
    this.password='';
  }

}
