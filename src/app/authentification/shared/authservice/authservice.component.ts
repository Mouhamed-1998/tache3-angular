import { Component ,Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/compat/auth";
import {Router} from '@angular/router'

@Component({
  selector: 'app-authservice',
  templateUrl: './authservice.component.html',
  styleUrls: ['./authservice.component.css']
})
@Injectable({
  providedIn:'root'
})

export class AuthserviceComponent {

constructor(private fireauth :  AngularFireAuth , private router : Router ){}

  //login
  login(email :  string, password : string){
    this.fireauth.signInWithEmailAndPassword(email,password).then(()=>{
      this.router.navigate(['dashboard'])

    },err=>{
      alert(err.message)
      this.router.navigate(['/connecte'])
    })
  }

  //registre

  register(email : string, password : string){
    this.fireauth.createUserWithEmailAndPassword(email,password).then(()=>{
      alert('inscription avec succée')
      this.router.navigate(['/connecte'])

    },err=>{
      alert(err.message)
      this.router.navigate(['/inscription'])
    })
  }

  // logout()
  logout(){
    this.fireauth.signOut().then(()=>{
      this.router.navigate(['/connecte'])
    },err=>{
      alert(err.message)
      
    }
    )
  }
 
}
