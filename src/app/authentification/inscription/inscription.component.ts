import { Component, OnInit } from '@angular/core';
import { AuthserviceComponent } from '../shared/authservice/authservice.component';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  email : string='';
 password : string ='' 
  constructor(private auth :AuthserviceComponent){
    
  }
 ngOnInit(): void {}
   

  register(){
    if(this.email == ''){
      alert('entrer votre email svp !')
      return;
    }
    if(this.password == ''){
      alert('entrer votre password svp !')
      return;
    }
    this.auth.register(this.email,this.password);
    this.email='';
    this.password='';
  }
 

}
